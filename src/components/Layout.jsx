import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Layout } from 'antd';

const Header = styled(Layout.Header)`
  position: fixed;
  width: 100%;
  z-index: 4;
`;

const Title = styled.h1`
  color: #fff;
`;

const Content = styled(Layout)`
  margin-top: 64px; /* To compensate fixed header */
`;

const Sider = styled(Layout.Sider)`
  background-color: #fff !important;
  height: calc(100vh - 64px); /* Compensate fixed header */
  position: fixed;
  left: 0;
  padding: 5px;
`;

const MapLayout = styled(Layout)`
  /* Compensate sider */
  margin-left: 300px;
`;

class MyLayout extends React.Component {
  static propTypes = {
    children: PropTypes.node.isRequired,
    sider: PropTypes.node.isRequired,
  };

  render() {
    return (
      <Layout>
        <Header>
          <Title>ThousandEyes interview project</Title>
        </Header>
        <Content>
          <Sider width={300}>{this.props.sider}</Sider>
          <MapLayout>
            <Layout.Content>{this.props.children}</Layout.Content>
          </MapLayout>
        </Content>
      </Layout>
    );
  }
}

export default MyLayout;
