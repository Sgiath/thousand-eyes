import React from 'react';
import styled from 'styled-components';

import DataInput from './DataInput';
import Settings from './Settings';
import Status from './Status';

const StatusWrapper = styled.div`
  position: absolute;
  bottom: 5px;
  width: calc(100% - 10px);
`;

class Sider extends React.Component {
  render() {
    return (
      <React.Fragment>
        <DataInput />
        <Settings />
        <StatusWrapper>
          <Status />
        </StatusWrapper>
      </React.Fragment>
    );
  }
}

export default Sider;
