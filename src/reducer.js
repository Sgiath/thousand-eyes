import { Record, List } from 'immutable';

import apiReducer from './api/reducer';
import settingsReducer from './settings/reducer';
import * as actions from './actions';

/**
 * Initial state for our Redux store
 */
const InitialState = Record({
  // Data from API - this will handle API reducer
  api: undefined,

  // Map settings - this will handle settings reducer
  settings: undefined,

  // Currently selected bus routes to display
  selectedRoutes: List(),
});

/**
 * Root reducer of the app
 */
export default function reducer(state = new InitialState(), action) {
  // Call sub-reducers
  state = state
    .update('api', (value) => apiReducer(value, action))
    .update('settings', (value) => settingsReducer(value, action));

  // Handle application actions
  switch (action.type) {
    case actions.selectRoutes.TYPE:
      return state.set('selectedRoutes', List(action.payload));
    default:
      return state;
  }
}
