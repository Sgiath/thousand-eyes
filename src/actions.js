import { makeActionCreator } from 'redux-toolbelt';

export const selectRoutes = makeActionCreator('SELECT_ROUTES');
