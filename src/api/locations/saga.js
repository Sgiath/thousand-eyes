import { delay } from 'redux-saga';
import { put, call } from 'redux-saga/effects';

import { fetchLocations } from './actions';
import { locations } from '../resources';

export default function* locationsSaga() {
  // Last time vehicles locations was fetched
  let time = '0';

  // Update indefinitely
  while (true) {
    // Dispatch fetch action
    yield put(fetchLocations());

    try {
      // Try to fetch vehicles locations
      const { vehicles, time: newTime } = yield call(locations, time);

      // Dispatch success action
      yield put(fetchLocations.success(vehicles));

      // Update last fetched time
      time = newTime;
    } catch (error) {
      // In case of error dispatch error action
      yield put(fetchLocations.failure(error));
    }

    // Wait 15 sec before next update
    yield delay(15000);
  }
}
