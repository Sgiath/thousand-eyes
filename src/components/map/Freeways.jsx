import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { geoFeature } from '../../geoUtils';

class Freeways extends React.Component {
  static propTypes = {
    data: PropTypes.object.isRequired,
    scale: PropTypes.number,
    color: PropTypes.string,
  };

  static defaultProps = {
    scale: 1,
    color: '#FCED56',
  };

  render() {
    const {
      data: { features },
      scale,
      color,
    } = this.props;

    // Generate streets
    const freeways = features.map((data, index) => (
      <path key={`freeway[${index}]`} d={geoFeature(data)} />
    ));

    const FreewaysStyled = styled.g`
      stroke-width: ${2 / scale}px;
      stroke: ${color};
      fill-opacity: 0;
    `;

    return <FreewaysStyled id="freeways">{freeways}</FreewaysStyled>;
  }
}

export default Freeways;
