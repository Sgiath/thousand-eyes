import { makeActionCreator } from 'redux-toolbelt';

export const changeTransform = makeActionCreator('@settings/TRANSLATE');
export const changeDimensions = makeActionCreator('@settings/DIMENSIONS');
export const showStops = makeActionCreator('@settings/SHOW_STOPS');
