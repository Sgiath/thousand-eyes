import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import Point from '../d3/Point';
import LineString from '../d3/LineString';
import { settingsSelector, transformSelector } from '../../selectors';

class Route extends React.Component {
  static propTypes = {
    paths: PropTypes.object.isRequired,
    stops: PropTypes.object.isRequired,
    name: PropTypes.string.isRequired,
    color: PropTypes.string.isRequired,
    oppositeColor: PropTypes.string.isRequired,
    tag: PropTypes.string.isRequired,
  };

  state = {
    width: 1,
  };

  render() {
    const {
      tag,
      paths,
      stops,
      transform: { k },
      color,
      oppositeColor,
    } = this.props;

    // Generate routes
    const routes = paths.map((path, index) => (
      <LineString
        key={`route[${tag}][${index}]`}
        coordinates={path.get('point').map((coo) => [coo.get('lon'), coo.get('lat')])}
        width={this.state.width}
        scale={k}
        color={color}
      />
    ));

    // Generate bus stops
    const stopPaths = stops.map((stop) => (
      <Point
        key={stop.get('stopId')}
        lon={stop.get('lon')}
        lat={stop.get('lat')}
        radius={1.5}
        scale={k}
        color={oppositeColor}
      />
    ));

    return (
      <g id={`busRoute[${tag}]`}>
        <g
          id="routes"
          onMouseEnter={() => this.setState({ width: 3 })}
          onMouseLeave={() => this.setState({ width: 1 })}
        >
          {routes}
        </g>
        {this.props.showStops && <g id="stops">{stopPaths}</g>}
      </g>
    );
  }
}

export default connect((state) => ({
  showStops: settingsSelector(state).get('showStops'),
  transform: transformSelector(state),
}))(Route);
