import { Record } from 'immutable';

import { fetchRoutes } from './actions';

/**
 * Initial state for routes reducer
 */
const InitialState = Record({
  isLoading: false,
  isError: false,
  data: null,
  error: null,
});

export default function routesReducer(state = new InitialState(), { type, payload }) {
  switch (type) {
    case fetchRoutes.TYPE:
      return state
        .set('isLoading', true)
        .remove('isError')
        .delete('error');
    case fetchRoutes.success.TYPE:
      return state
        .remove('isLoading')
        .remove('isError')
        .delete('error')
        .set('data', payload);
    case fetchRoutes.failure.TYPE:
      return state
        .remove('isLoading')
        .set('isError', true)
        .set('error', payload);
    default:
      return state;
  }
}
