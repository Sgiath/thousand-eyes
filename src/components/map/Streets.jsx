import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { geoFeature } from '../../geoUtils';

class Streets extends React.Component {
  static propTypes = {
    data: PropTypes.object.isRequired,
    scale: PropTypes.number,
    color: PropTypes.string,
  };

  static defaultProps = {
    scale: 1,
    color: 'rgba(0, 0, 0, 0.08)',
  };

  render() {
    const {
      data: { features },
      scale,
      color,
    } = this.props;

    // Generate streets
    const streets = features.map((data, index) => (
      <path key={`street[${index}]`} d={geoFeature(data)} />
    ));

    const StreetsStyled = styled.g`
      stroke-width: ${1 / scale}px;
      stroke: ${color};
      fill-opacity: 0;
    `;

    return <StreetsStyled id="streets">{streets}</StreetsStyled>;
  }
}

export default Streets;
