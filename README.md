# ThousandEyes interview project

Live version: https://thousandeyes.sgiath.net/

## How I proceeded

### Preparation

* I choose React as the main framework because I am most familiar with it.
* I integrated D3 with React in way that React is handling the DOM and D3 is responsible
  just for the calculations. It feels more natural for me to let React handle all of the
  DOM and I have more experience with DOM manipulating in React then in D3. React has
  also more features I can use. This approach has one downside that there are not many
  examples on internet demonstrating this approach (the React usage is usually
  demonstrated with [react-d3](https://github.com/react-d3/react-d3-map) library which
  is 2 years old, not maintained and do not work with latest React).
  * More info about this approach is in [this article](https://medium.com/@Elijah_Meeks/interactive-applications-with-react-d3-f76f7b3ebc71#8298).

### Step 1

* Initiate project with [create-react-app](https://github.com/facebook/create-react-app).
* Get D3-Geo working (draw the SF map).
  * I try to use [react-d3](https://github.com/react-d3/react-d3-map) but found out that
    the last commit was 2 years ago and it is not working with current React version. I
    tried to fix the library but I quickly realize that it would be easier to just
    integrate D3 to React by myself (I maybe try to fix it after I finish the project).
* Get some data from the [nextbus.com](http://webservices.nextbus.com/) and implement
  fetching them and storing in the Redux store.
* Initiate some basic layout and UI with [Antd](https://ant.design/) (I choose this
  library because it has ton of advanced UI elements - especially tags).
* Implement Agency/Route selector and show associated bus stops on the map.

_Note:_ only routes for agency San Francisco Muni are actually on the map other agencies
seems to be outside of San Francisco => ask if only San Francisco Muni should be taken
into account or if any other agencies have routes inside San Francisco.

**Commit:** [6101b9bb](https://gitlab.com/Sgiath/thousand-eyes/tree/6101b9bb6bed2f64f62add6b1cfedd585163851b)

### Step 2

* Remove agencies fetcher and selector and use just `sf-muni` agency.
* Get all routes config by one request.
* Implement tag input and show routes for selected lines on the map with appropriate
  color from the API.
* Show also position of the bus stops on the route.

_Note:_ I found out that it is in the project description that I should use just
San Francisco Muni agency (derp).

**Commit:** [2f8b4a4b](https://gitlab.com/Sgiath/thousand-eyes/tree/2f8b4a4b9feada1567dff9a5efd5b05c6dd120e9)

### Step 3

* Add vehicle location resource and implement re-fetching it in 15 sec interval.
* Show vehicle position on the map filtered by selected routes.
* Refactor d3-geo configuration and put it into separate `geoUtils.js` file.

**Commit:** [9a31ca78](https://gitlab.com/Sgiath/thousand-eyes/tree/9a31ca78fecc54088f00a7551f5d01d87dc6d2cd)

### Step 4

* Add settings to show or hide bus stops.
* Add error message when API call fails (with option to refetch the data).
* Add nice UI spinner while loading map data.
* Add Online/Offline status for vehicle locations.

**Commit:** [1ce085b4](https://gitlab.com/Sgiath/thousand-eyes/tree/1ce085b40642430a567e16c9dca5a9e655fc6a90)

### Step 5

* Add zooming and moving features to the map.
* Add freeways and arteries to the map.
* Because of bad performance when zooming or moving the map I decided show streets only
  on higher zoom level.
* Add neighborhoods labels.
* Automatic width/height detection.
* Add CI/CD configuration.

**Commit:** [de93e0a6](https://gitlab.com/Sgiath/thousand-eyes/tree/de93e0a6ce2ea4d304a0b5042a512e31e7de2d89)

### Step 6

* Prepare for deploy on Firebase Hosting and deploy it.
* It is not working deployed because NextBus only provides HTTP API endpoint and it is
  not allowed use HTTP request from HTTPS page => I decided to create simple Firebase
  Function which will wrap the HTTP request in HTTPS.
* Write Firebase Function for API requests (folder `/functions`). I decided to use HTTP
  based function instead of function call because I didn't want to introduce new
  dependency for my frontend app (`firebase`) for just two API calls. For bigger app I
  would consider using function call instead.

**Commit:** [b4aa7e1e](https://gitlab.com/Sgiath/thousand-eyes/tree/b4aa7e1e8b107299ae143c0781ca3d84bef3a849)

### Step 7

* Final refactoring and edits
* Add some comments
* Switch Firebase Functions to TypeScript (I just wanted to try it)
* Optimize app size

**Commit:** [a42141fd](https://gitlab.com/Sgiath/thousand-eyes/tree/a42141fd2bda1b826af13aec79fbda644316dd86)
