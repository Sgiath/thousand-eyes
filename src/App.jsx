import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';

import Layout from './components/Layout';
import Canvas from './components/Canvas';
import Sider from './components/sider/Sider';
import { changeDimensions } from './settings/actions';

class App extends React.Component {
  // @ts-ignore
  container = React.createRef();

  componentDidMount() {
    if (this.container.current) {
      this.props.changeDimensions({
        width: this.container.current.clientWidth,
        height: this.container.current.clientHeight,
      });
    }
  }

  componentDidUpdate() {
    if (this.container.current) {
      this.props.changeDimensions({
        width: this.container.current.clientWidth,
        height: this.container.current.clientHeight,
      });
    }
  }

  render() {
    /**
     * Created to measure available space for map
     */
    const DimensionTester = styled.div`
      position: fixed;
      /* Sidebar */
      left: 300px;
      /* Header */
      top: 64px;
      right: 0;
      bottom: 0;
      z-index: -1;
    `;

    return (
      <Layout sider={<Sider />}>
        <DimensionTester innerRef={this.container} />
        <Canvas />
      </Layout>
    );
  }
}

export default connect(
  (state) => ({}),
  (dispatch) => ({
    changeDimensions: ({ width, height }) =>
      dispatch(changeDimensions({ width, height })),
  }),
)(App);
