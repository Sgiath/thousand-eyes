import React from 'react';
import { connect } from 'react-redux';
import { zoom } from 'd3-zoom';
import { event, select } from 'd3-selection';
import { Spin } from 'antd';

import Routes from './bus/Routes';
import Vehicles from './bus/Vehicles';
import BaseMap from './map/BaseMap';
import { transformSelector, dimensionsSelector } from '../selectors';
import { changeTransform } from '../settings/actions';

class Canvas extends React.Component {
  constructor(props) {
    super(props);

    this.zoom = zoom().on('zoom', this.handleZoom);
    // @ts-ignore
    this.svg = React.createRef();
  }

  state = {
    neighborhoods: null,
    freeways: null,
    arteries: null,
    streets: null,
  };

  componentDidMount() {
    // @ts-ignore
    import('../data/neighborhoods.json').then((data) => {
      this.setState({ neighborhoods: data });
    });
    // @ts-ignore
    import('../data/freeways.json').then((data) => {
      this.setState({ freeways: data });
    });
    // @ts-ignore
    import('../data/arteries.json').then((data) => {
      this.setState({ arteries: data });
    });
    // @ts-ignore
    import('../data/streets.json').then((data) => {
      this.setState({ streets: data });
    });
  }

  componentDidUpdate() {
    if (this.svg.current) {
      select(this.svg.current).call(this.zoom);
    }
  }

  handleZoom = () => {
    this.props.changeTransform(event.transform);
  };

  render() {
    if (
      this.state.neighborhoods === null ||
      this.state.streets === null ||
      this.state.freeways === null ||
      this.state.arteries === null
    ) {
      return (
        <div style={{ textAlign: 'center', marginTop: 200 }}>
          <Spin size="large" />
        </div>
      );
    }

    const {
      transform,
      dim: { width, height },
    } = this.props;
    return (
      <svg ref={this.svg} width={width} height={height} onDoubleClick={this.handleZoom}>
        <g transform={transform}>
          <BaseMap {...this.state} />
          <Routes />
          <Vehicles />
        </g>
      </svg>
    );
  }
}

export default connect(
  (state) => ({
    transform: transformSelector(state),
    dim: dimensionsSelector(state),
  }),
  (dispatch) => ({
    changeTransform: (transform) => dispatch(changeTransform(transform)),
  }),
)(Canvas);
