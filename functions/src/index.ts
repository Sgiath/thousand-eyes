import * as functions from 'firebase-functions';
import * as http from 'http';

const agent = new http.Agent({ keepAlive: true });

export const nextbus = functions.https.onRequest((request, response) => {
  const { t = '0', a = 'sf-muni' } = request.query;

  const routes = `command=routeConfig&a=${a}`;
  const vehicles = `command=vehicleLocations&a=${a}&${t}`;

  response.header('Access-Control-Allow-Origin', '*');

  const { path } = request;
  let query: string;
  if (path === '/routes') {
    query = routes;
  } else if (path === '/vehicles') {
    query = vehicles;
  } else {
    console.warn(`Path ${path} do not exists`)
    response.status(404).send();
    return;
  }

  const req = http.request(
    {
      host: 'webservices.nextbus.com',
      path: `/service/publicJSONFeed?${query}`,
      port: 80,
      method: 'GET',
      agent: agent,
    },
    (res) => {
      const { statusCode } = res;

      if (statusCode !== 200) {
        console.warn(`Status code from NextBus ${statusCode}`)
        response.send(statusCode).send();
      }

      res.setEncoding('utf8');
      let rawData: string = '';
      res.on('data', (chunk: string) => {
        rawData += chunk;
      });

      res.on('end', () => response.status(200).type('json').send(rawData));
    }
  );

  req.on('error', (error) => {
    console.error(error);
    response.status(500).send(error.message);
  });
  
  req.end();
});
