import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { composeWithDevTools } from 'redux-devtools-extension';

import reducer from './reducer';
import saga from './saga';

/**
 * Redux store object
 */
let store;

/**
 * Configure Redux store object (or just return already configured)
 */
export default function configureStore() {
  if (store !== undefined) {
    return store;
  }

  const sagaMiddleware = createSagaMiddleware();
  store = createStore(reducer, composeWithDevTools(applyMiddleware(sagaMiddleware)));
  sagaMiddleware.run(saga);

  return store;
}
