import React from 'react';
import { connect } from 'react-redux';
import { Select, Alert, Button, Icon, Spin } from 'antd';

import { fetchRoutes } from '../../api/routes/actions';
import { selectRoutes } from '../../actions';
import { routesSelector } from '../../selectors';

class DataInput extends React.Component {
  componentDidMount() {
    this.props.fetchRoutes();
  }

  handleChange = (value) => {
    this.props.selectRoutes(value);
  };

  render() {
    const { routes } = this.props;

    if (routes.get('isError')) {
      return (
        <React.Fragment>
          <Alert type="error" message="Failed to get list of routes" showIcon />
          <Button
            type="primary"
            onClick={this.props.fetchRoutes}
            style={{ marginTop: 5 }}
          >
            <Icon type="sync" /> Refetch
          </Button>
        </React.Fragment>
      );
    }

    if (routes.get('data') === null) {
      return (
        <div style={{ textAlign: 'center', marginTop: 10 }}>
          <Spin />
        </div>
      );
    }

    const options = routes.get('data').map((route) => (
      <Select.Option
        value={route.get('tag')}
        key={route.get('tag')}
        style={{ color: `#${route.get('color')}` }}
      >
        {route.get('title')}
      </Select.Option>
    ));
    return (
      <React.Fragment>
        <Select
          mode="tags"
          style={{ width: '100%' }}
          placeholder="Select routes"
          onChange={this.handleChange}
        >
          {options}
        </Select>
        <Button
          onClick={() =>
            this.props.selectRoutes(
              this.props.routes.get('data').map((route) => route.get('tag')),
            )
          }
        >
          Select all
        </Button>
        <Button onClick={() => this.props.selectRoutes([])}>Deselect all</Button>
      </React.Fragment>
    );
  }
}

export default connect(
  (state) => ({
    routes: routesSelector(state),
  }),
  (dispatch) => ({
    fetchRoutes: () => dispatch(fetchRoutes()),
    selectRoutes: (routes) => dispatch(selectRoutes(routes)),
  }),
)(DataInput);
