import { Record } from 'immutable';
import { zoomIdentity } from 'd3-zoom';

import * as actions from './actions';

/**
 * Initial state for the map settings
 */
const InitialState = Record({
  showStops: false,
  // Default dimensions
  width: 960,
  height: 500,
  // San Francisco coordinates (from Wikipedia)
  center: [-122.416667, 37.783333],
  // Scale found by experiment
  scale: 210000,
  // Start with no transform
  transform: zoomIdentity,
});

/**
 * Settings reducer
 */
export default function settingsReducer(state = new InitialState(), { type, payload }) {
  switch (type) {
    case actions.showStops.TYPE:
      return state.set('showStops', payload);
    case actions.changeTransform.TYPE:
      return state.set('transform', payload);
    case actions.changeDimensions.TYPE:
      return state.set('width', payload.width).set('height', payload.height);
    default:
      return state;
  }
}
