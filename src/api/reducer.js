import { Record } from 'immutable';

import routesReducer from './routes/reducer';
import locationsReducer from './locations/reducer';

/**
 * Initial state for API reducer
 */
const InitialState = Record({
  // State subset for routes
  routes: undefined,

  // State subset for location
  locations: undefined,
});

/**
 * API reducer
 */
export default function apiReducer(state = new InitialState(), action) {
  return (
    state
      // Call routes reducer
      .update('routes', (value) => routesReducer(value, action))
      // Call locations reducer
      .update('locations', (value) => locationsReducer(value, action))
  );
}
