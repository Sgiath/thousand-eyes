import React from 'react';
import PropTypes from 'prop-types';

import { getGenerator } from '../../geoUtils';

class Point extends React.Component {
  static propTypes = {
    radius: PropTypes.number,
    color: PropTypes.string,
    scale: PropTypes.number,
    lon: PropTypes.string.isRequired,
    lat: PropTypes.string.isRequired,
  };

  static defaultProps = {
    radius: 4.5,
    scale: 1,
    color: '#000000',
  };

  render() {
    const { lon, lat, radius, scale, color } = this.props;

    // Get GeoJSON representation
    const geoJson = {
      type: 'Feature',
      geometry: {
        type: 'Point',
        coordinates: [lon, lat],
      },
    };

    // Compute data for path
    // @ts-ignore
    const d = getGenerator().pointRadius(radius / scale)(geoJson);

    return <path d={d} fill={color} />;
  }
}

export default Point;
