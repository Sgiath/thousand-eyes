import { makeAsyncActionCreator } from 'redux-toolbelt';

export const fetchLocations = makeAsyncActionCreator('@api/FETCH_LOCATIONS');
