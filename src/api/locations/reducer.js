import { Record, List } from 'immutable';

import { fetchLocations } from './actions';

/**
 * Initial state for Locations reducer
 */
const InitialState = Record({
  isLoading: false,
  isError: false,
  data: List(),
  error: null,
});
export default function locationsReducer(
  state = new InitialState(),
  { type, payload },
) {
  switch (type) {
    case fetchLocations.TYPE:
      return state.set('isLoading', true);
    case fetchLocations.success.TYPE:
      return state
        .remove('isLoading')
        .remove('isError')
        .delete('error')
        .update('data', (data) => {
          // Get IDs of changed vehicles
          const ids = payload.map((vehicle) => vehicle.get('id'));
          // Filter vehicles not changed
          const notChanged = data.filter((vehicle) => !ids.includes(vehicle.get('id')));
          // Join not changed and changed vehicles
          return notChanged.concat(payload);
        });
    case fetchLocations.failure.TYPE:
      return state
        .remove('isLoading')
        .set('isError', true)
        .set('error', payload);
    default:
      return state;
  }
}
