import { take, put, call } from 'redux-saga/effects';

import { fetchRoutes } from './actions';
import { routes } from '../resources';

export default function* routesSaga() {
  while (true) {
    // Wait for fetch action
    yield take(fetchRoutes.TYPE);

    try {
      // Try to call API
      const data = yield call(routes);

      // Dispatch success action
      yield put(fetchRoutes.success(data));
    } catch (error) {
      // In case of error dispatch error action
      yield put(fetchRoutes.failure(error));
    }
  }
}
