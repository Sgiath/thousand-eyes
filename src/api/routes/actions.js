import { makeAsyncActionCreator } from 'redux-toolbelt';

export const fetchRoutes = makeAsyncActionCreator('@api/FETCH_ROUTES');
