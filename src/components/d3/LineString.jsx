import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { getGenerator } from '../../geoUtils';

class LineString extends React.Component {
  static propTypes = {
    width: PropTypes.number,
    color: PropTypes.string,
    scale: PropTypes.number,
    coordinates: PropTypes.object.isRequired,
  };

  static defaultProps = {
    width: 1,
    scale: 1,
    color: '#000000',
  };

  render() {
    const { coordinates, width, scale, color } = this.props;

    // Get GeoJSON representation
    const geoJson = {
      type: 'Feature',
      geometry: {
        type: 'LineString',
        coordinates: coordinates.toJS(),
      },
    };

    // Compute data for path
    // @ts-ignore
    const d = getGenerator()(geoJson);

    const LineStringStyled = styled.path`
      fill-opacity: 0;
      stroke-width: ${width / scale}px;
    `;

    return <LineStringStyled d={d} stroke={color} />;
  }
}

export default LineString;
