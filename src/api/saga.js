import { all } from 'redux-saga/effects';

import routesSaga from './routes/saga';
import locationsSaga from './locations/saga';

export default function* apiSaga() {
  yield all([routesSaga(), locationsSaga()]);
}
