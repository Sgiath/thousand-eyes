import React from 'react';
import { connect } from 'react-redux';
import { zoomIdentity } from 'd3-zoom';
import styled from 'styled-components';
import { Button, Checkbox } from 'antd';

import { settingsSelector } from '../../selectors';
import { showStops, changeTransform } from '../../settings/actions';

const SettingsWrapper = styled.div`
  margin-top: 50px;
`;

const ResetButton = styled(Button)`
  margin-top: 10px;
`;

class Settings extends React.Component {
  render() {
    const { settings } = this.props;

    return (
      <SettingsWrapper>
        <h3>Settings</h3>
        <Checkbox
          checked={settings.get('showStops')}
          onChange={(event) => this.props.showStops(event.target.checked)}
        >
          Show bus stops
        </Checkbox>
        <br />
        <ResetButton onClick={this.props.resetMap} type="primary">
          Reset map
        </ResetButton>
      </SettingsWrapper>
    );
  }
}

export default connect(
  (state) => ({
    settings: settingsSelector(state),
  }),
  (dispatch) => ({
    showStops: (value) => dispatch(showStops(value)),
    resetMap: () => dispatch(changeTransform(zoomIdentity)),
  }),
)(Settings);
