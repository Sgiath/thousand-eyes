import axios from 'axios';
import { fromJS } from 'immutable';

/**
 * GET request wrapper
 *
 * @param {string} resource API resource ('routes' or 'vehicles')
 * @param {object} params Additional parameters (right now just optional 't' parameter
 *                        for 'vehicles' resource)
 */
export default function getRequest(resource, params = {}) {
  return axios({
    method: 'GET',
    baseURL: 'https://us-central1-thousand-eyes-94a71.cloudfunctions.net/',
    url: `/nextbus/${resource}`,
    params,

    // Auto parse to JSON
    responseType: 'json',

    // Convert received data to Immutable structure
    transformResponse: (data) => fromJS(data),
  }).then((response) => {
    // Extract just data from the response
    return response.data;
  });
}
