export function selectedRoutesSelector(state) {
  return state.get('selectedRoutes');
}

export function routesSelector(state) {
  return state.getIn(['api', 'routes']);
}

export function locationsSelector(state) {
  return state.getIn(['api', 'locations']);
}

export function settingsSelector(state) {
  return state.get('settings');
}

export function transformSelector(state) {
  return state.getIn(['settings', 'transform']);
}

export function dimensionsSelector(state) {
  return {
    width: state.getIn(['settings', 'width']),
    height: state.getIn(['settings', 'height']),
  };
}
