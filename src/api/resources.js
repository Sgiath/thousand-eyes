import request from './request';

/**
 * Request for the bus routes config
 *
 * Calls our Firebase Function which then calls NextBus API command routeConfig
 */
export function routes() {
  return request('routes').then((data) => {
    // Extract just data we care about
    return data.get('route');
  });
}

/**
 * Request for the vehicles locations
 *
 * Calls our Firebase Function which then calls NextBus API command vehicleLocations
 *
 * @param {string} time Last time API was called
 */
export function locations(time = '0') {
  return request('vehicles', { t: time }).then((data) => {
    return {
      // Data we care about
      vehicles: data.get('vehicle'),

      // Time for the next API call
      time: data.getIn(['lastTime', 'time']),
    };
  });
}
