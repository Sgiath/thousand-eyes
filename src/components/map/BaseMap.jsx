import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import Neighborhoods from './Neighborhoods';
import Streets from './Streets';
import Freeways from './Freeways';
import Arteries from './Arteries';
import { transformSelector } from '../../selectors';

class BaseMap extends React.Component {
  static propTypes = {
    streets: PropTypes.object.isRequired,
    freeways: PropTypes.object.isRequired,
    arteries: PropTypes.object.isRequired,
    neighborhoods: PropTypes.object.isRequired,
  };

  render() {
    const {
      transform: { k },
    } = this.props;

    return (
      <g className="map">
        <Neighborhoods data={this.props.neighborhoods} scale={k} />
        <Freeways data={this.props.freeways} scale={k} />
        <Arteries data={this.props.arteries} scale={k} />
        {k > 3 && <Streets data={this.props.streets} scale={k} />}
      </g>
    );
  }
}

export default connect((state) => ({
  transform: transformSelector(state),
}))(BaseMap);
