import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { geoFeature } from '../../geoUtils';

class Arteries extends React.Component {
  static propTypes = {
    data: PropTypes.object.isRequired,
    scale: PropTypes.number,
    color: PropTypes.string,
  };

  static defaultProps = {
    scale: 1,
    color: '#EADD5D',
  };

  render() {
    const {
      data: { features },
      scale,
      color,
    } = this.props;

    // Generate streets
    const arteries = features.map((data, index) => (
      <path key={`arterie[${index}]`} d={geoFeature(data)} />
    ));

    const ArteriesStyled = styled.g`
      stroke: ${color};
      stroke-width: ${1.5 / scale};
      fill-opacity: 0;
    `;

    return <ArteriesStyled id="arteries">{arteries}</ArteriesStyled>;
  }
}

export default Arteries;
