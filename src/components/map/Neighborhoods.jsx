import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { geoFeature, getGenerator } from '../../geoUtils';

// Text component for Label
const Label = styled.text`
  opacity: 0.7;
  stroke: #000;
  stroke-width: 0.25px;
  text-anchor: middle;
`;

class Neighborhoods extends React.Component {
  static propTypes = {
    data: PropTypes.object.isRequired,
    scale: PropTypes.number,
    color: PropTypes.shape({
      fill: PropTypes.string.isRequired,
      stroke: PropTypes.string.isRequired,
    }),
  };

  static defaultProps = {
    scale: 1,
    color: {
      fill: '#dcd8d2',
      stroke: '#c9c4bc',
    },
  };

  render() {
    const {
      data: { features },
      scale,
      color: { fill, stroke },
    } = this.props;

    // Generate neighborhoods
    const neighborhoods = features.map((data, index) => {
      const d = geoFeature(data);
      const center = getGenerator().centroid(data);
      return (
        <g key={data.properties.neighborho}>
          <path d={d} />
          <Label transform={`translate(${center})`}>{data.properties.neighborho}</Label>
        </g>
      );
    });

    // Group component for neighborhoods
    const NeighborhoodsGroup = styled.g`
      stroke-width: ${2 / scale}px;
      fill: ${fill};
      stroke: ${stroke};
    `;

    return <NeighborhoodsGroup id="neighborhoods">{neighborhoods}</NeighborhoodsGroup>;
  }
}

export default Neighborhoods;
