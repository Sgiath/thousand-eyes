import React from 'react';
import { connect } from 'react-redux';

import Route from './Route';
import { selectedRoutesSelector, routesSelector } from '../../selectors';

class Routes extends React.Component {
  render() {
    const { routes, selectedRoutes } = this.props;

    // If data are not loaded show nothing
    if (!routes.get('data')) {
      return null;
    }

    // Filter just selected routes
    const showRoutes = routes
      .get('data')
      .filter((route) => selectedRoutes.includes(route.get('tag')));

    // Generate
    const children = showRoutes.map((route) => (
      <Route
        key={route.get('tag')}
        // @ts-ignore
        tag={route.get('tag')}
        name={route.get('title')}
        color={`#${route.get('color')}`}
        paths={route.get('path')}
        stops={route.get('stop')}
        oppositeColor={`#${route.get('oppositeColor')}`}
      />
    ));

    return <g id="busRoutes">{children}</g>;
  }
}

export default connect((state) => ({
  routes: routesSelector(state),
  selectedRoutes: selectedRoutesSelector(state),
}))(Routes);
