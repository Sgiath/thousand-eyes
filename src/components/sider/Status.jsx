import React from 'react';
import { connect } from 'react-redux';
import { Alert } from 'antd';

import { locationsSelector } from '../../selectors';

class Status extends React.Component {
  render() {
    if (this.props.online) {
      return <Alert message="Online" type="success" style={{ textAlign: 'center' }} />;
    }
    return <Alert message="Offline" type="error" style={{ textAlign: 'center' }} />;
  }
}

export default connect((state) => ({
  online: !locationsSelector(state).get('isError'),
}))(Status);
