import React from 'react';
import { connect } from 'react-redux';
import { Map } from 'immutable';

import Point from '../d3/Point';
import {
  selectedRoutesSelector,
  locationsSelector,
  transformSelector,
  routesSelector,
} from '../../selectors';

class Vehicles extends React.Component {
  render() {
    const {
      locations,
      selectedRoutes,
      transform: { k },
      routes,
    } = this.props;

    if (routes.get('data') === null) {
      return null;
    }

    // Filter just vehicles for selected routes
    const data = locations.filter((location) =>
      selectedRoutes.includes(location.get('routeTag')),
    );

    const colors = routes
      .get('data')
      .reduce((acc, value) => acc.set(value.get('tag'), value.get('color')), Map());

    // Generate vehicles marks
    const paths = data.map((location) => (
      <Point
        key={location.get('id')}
        lon={location.get('lon')}
        lat={location.get('lat')}
        radius={3}
        scale={k}
        color={'#' + colors.get(location.get('routeTag'))}
      />
    ));

    return <g id="vehicles">{paths}</g>;
  }
}

export default connect((state) => ({
  locations: locationsSelector(state).get('data'),
  selectedRoutes: selectedRoutesSelector(state),
  transform: transformSelector(state),
  routes: routesSelector(state),
}))(Vehicles);
