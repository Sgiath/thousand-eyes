import { geoMercator, geoPath } from 'd3-geo';

import store from './configureStore';
import { dimensionsSelector, settingsSelector } from './selectors';

/**
 * Create geoPath generator from current state
 */
export function getGenerator() {
  const state = store().getState();

  const settings = settingsSelector(state);
  const { width, height } = dimensionsSelector(state);

  const projection = geoMercator()
    .center(settings.get('center'))
    // Translate map slightly higher then center (looks better)
    .translate([width / 2, height / 2 - 100])
    .scale(settings.get('scale'));

  return geoPath().projection(projection);
}

/**
 * Generate path from the map Feature
 */
export function geoFeature(feature) {
  return getGenerator()(feature);
}
